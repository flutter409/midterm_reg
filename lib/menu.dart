import 'package:flutter/material.dart';
import '../main.dart';
import 'timetable.dart';
import 'personal_stu.dart.';
import 'login.dart';

class MyMenu extends StatelessWidget {
  List<Map> menu = [
    {'icon': Icons.home, 'title': 'หน้าแรก'},
    {'icon': Icons.note_alt, 'title': 'ลงทะเบียน'},
    {'icon': Icons.notes_rounded, 'title': 'ตารางเรียน'},
    {'icon': Icons.person, 'title': 'ประวัตินิสิต'},
    {'icon': Icons.exit_to_app, 'title': 'ออกจากระบบ'},
  ];

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          const UserAccountsDrawerHeader(
            decoration: BoxDecoration(color: Colors.white),
            accountName: Text(
              "นรารัตน์ ลิขิตธนานันท์",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            accountEmail: Text(
              "63160202@go.buu.ac.th",
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
            ),
            currentAccountPicture: CircleAvatar(
              backgroundImage: NetworkImage(
                './images/p1.png',
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: menu
                .map((element) => Padding(
                      padding: const EdgeInsets.all(0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          ElevatedButton.icon(
                              style: ElevatedButton.styleFrom(
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(1.0)),
                                minimumSize: Size(310, 60),
                                backgroundColor: Colors.white, //////// HERE
                              ),
                              onPressed: () {
                                if (menu.indexOf(element) == 0) {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => HomePage2()),
                                  );
                                } else if (menu.indexOf(element) == 1) {
                                  showDialog(
                                      context: context,
                                      builder: (context) => AlertDialog(
                                            actions: [
                                              TextButton(
                                                  onPressed: () {
                                                    Navigator.pop(context);
                                                  },
                                                  child:
                                                      Text('กลับสู่หน้าหลัก'))
                                            ],
                                            title:
                                                const Text('reg.buu.ac.th :'),
                                            content: const Text(
                                                'นิสิตทำการลงทะเบียนเรียบร้อยแล้ว'),
                                          ));
                                } else if (menu.indexOf(element) == 2) {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => timetable()),
                                  );
                                } else if (menu.indexOf(element) == 3) {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => personal()),
                                  );
                                } else if (menu.indexOf(element) == 4) {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Login()),
                                  );
                                }
                              },
                              icon: Icon(
                                element['icon'],
                                color: Colors.black,
                                size: 15,
                              ),
                              label: Text(element['title'],
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 15)))
                        ],
                      ),
                    ))
                .toList(),
          ),
        ],
      ),
    );
  }
}
