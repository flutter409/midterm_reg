import 'package:flutter/material.dart';
import '../main.dart';

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white70,
      body: Center(
        child: ListView(
          children: <Widget>[
            Container(
              height: 30,
            ),
            Container(
              width: 200,
              height: 150,
              child: Image.asset("./images/l1.png"),
            ),
            Container(
                padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
                child: TextField(
                  decoration: new InputDecoration(
                    border: new OutlineInputBorder(
                        borderSide: new BorderSide(color: Colors.white38)),
                    hintText: 'Enter your User',
                    helperText: 'กรุณาใส่ชื่อผู้ใช้ให้ถูกต้อง',
                    labelText: 'User Name',
                    prefixIcon: const Icon(
                      Icons.person,
                      color: Colors.amber,
                    ),
                  ),
                )),
            Container(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: TextField(
                obscureText: true,
                decoration: new InputDecoration(
                  border: new OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.white38)),
                  hintText: 'Enter your password',
                  helperText: 'กรุณาใส่รหัสให้ถูกต้อง',
                  labelText: 'Password',
                  prefixIcon: const Icon(
                    Icons.person,
                    color: Colors.amber,
                  ),
                ),
              ),
            ),
            Container(
                height: 80,
                padding: const EdgeInsets.all(20),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0)),
                    minimumSize: const Size.fromHeight(50),
                    backgroundColor: Colors.amber,
                  ),
                  child: const Text(
                    'LOG IN',
                    style: TextStyle(color: Colors.black),
                  ),
                  onPressed: () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => HomePage2(),
                      ),
                    );
                  },
                )),
            TextButton(
              onPressed: () {
                print("click");
                showDialog<String>(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                    title: const Text('ลืมรหัสผ่าน'),
                    content: const Text('คุณต้องการเปลี่ยนรหัสผ่านหรือไม่'),
                    actions: <Widget>[
                      TextButton(
                        onPressed: () => Navigator.pop(context),
                        child: const Text('ยกเลิก'),
                      ),
                      TextButton(
                        onPressed: () => Navigator.pop(context),
                        child: const Text('ตกลง'),
                      ),
                    ],
                  ),
                );
              },
              child: Text(
                'ลืมรหัสผ่าน?',
                style: TextStyle(color: Colors.black),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
