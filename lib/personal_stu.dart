import 'package:flutter/material.dart';
import '../main.dart';

class personal extends StatefulWidget {
  @override
  _personal createState() => _personal();
}

class _personal extends State<personal> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white38,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text('ประวัตินิสิต',
            style:
                TextStyle(fontSize: 20, color: Color.fromARGB(255, 0, 0, 0))),
      ),
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              children: [const SizedBox(height: 30), personalWidget()],
            ),
          )
        ],
      ),
    );
  }

  Widget personalWidget() {
    return Column(
      children: [
        Container(
          width: 350,
          height: 30,
          color: Color.fromARGB(255, 255, 243, 120),
          child: Row(children: [
            SizedBox(width: 10),
            Text(
              "ข้อมูลการศึกษา",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ]),
        ),
        SizedBox(height: 10),
        Row(children: [
          SizedBox(width: 40),
          Text("รหัสประจำตัว:",
              style: TextStyle(
                fontSize: 15,
              )),
          SizedBox(width: 65),
          Text("63160202"),
        ]),
        SizedBox(height: 10),
        Row(children: [
          SizedBox(width: 40),
          Text("ชื่อ:",
              style: TextStyle(
                fontSize: 15,
              )),
          SizedBox(width: 125),
          Text("นางสาวนรารัตน์ ลิขิตธนานันท์"),
        ]),
        SizedBox(height: 10),
        Row(children: [
          SizedBox(width: 40),
          Text("คณะ:",
              style: TextStyle(
                fontSize: 15,
              )),
          SizedBox(width: 116),
          Text("คณะวิทยาการสารสนเทศ"),
        ]),
        SizedBox(height: 10),
        Row(children: [
          SizedBox(width: 40),
          Text("วิทยาเขต:",
              style: TextStyle(
                fontSize: 15,
              )),
          SizedBox(width: 90),
          Text("บางแสน"),
        ]),
        SizedBox(height: 10),
        Row(children: [
          SizedBox(width: 40),
          Text("หลักสูตร:",
              style: TextStyle(
                fontSize: 15,
              )),
          SizedBox(width: 92),
          Text("2115020 วท.บ. 4 ปี ปกติ"),
        ]),
        SizedBox(height: 10),
        Row(children: [
          SizedBox(width: 40),
          Text("ระดับการศึกษา",
              style: TextStyle(
                fontSize: 15,
              )),
          SizedBox(width: 60),
          Text("ปริญญาตรี"),
        ]),
        SizedBox(height: 10),
        Row(children: [
          SizedBox(width: 40),
          Text("ชื่อปริญญา:",
              style: TextStyle(
                fontSize: 15,
              )),
          SizedBox(width: 78),
          Text("วิทยาศาสตรบัณฑิต วท.บ."),
        ]),
        SizedBox(height: 10),
        Row(children: [
          SizedBox(width: 40),
          Text("ปีการศึกษาที่เข้า:",
              style: TextStyle(
                fontSize: 15,
              )),
          SizedBox(width: 53),
          Text("2563 / 1 วันที่ 21/5/2563"),
        ]),
        SizedBox(height: 10),
        Row(children: [
          SizedBox(width: 40),
          Text("วิธีรับเข้า:",
              style: TextStyle(
                fontSize: 15,
              )),
          SizedBox(width: 95),
          Text("รับตรงทั่วประเทศ"),
        ]),
        SizedBox(height: 10),
        Row(children: [
          SizedBox(width: 40),
          Text("วุฒิก่อนเข้ารับการศึกษา:",
              style: TextStyle(
                fontSize: 15,
              )),
          SizedBox(width: 10),
          Text("ม.6"),
        ]),
        SizedBox(height: 15),
        Container(
          width: 350,
          height: 30,
          color: Color.fromARGB(255, 255, 243, 120),
          child: Row(children: [
            SizedBox(width: 10),
            Text(
              "ผลการศึกษา",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ]),
        ),
        SizedBox(height: 10),
        Row(children: [
          SizedBox(width: 40),
          Text("หน่วยกิตคำนวณ:",
              style: TextStyle(
                fontSize: 15,
              )),
          SizedBox(width: 45),
          Text("96"),
        ]),
        SizedBox(height: 10),
        Row(children: [
          SizedBox(width: 40),
          Text("หน่วยกิตที่ผ่าน:",
              style: TextStyle(
                fontSize: 15,
              )),
          SizedBox(width: 56),
          Text("93"),
        ]),
        SizedBox(height: 10),
        Row(children: [
          SizedBox(width: 40),
          Text("คะแนนเฉลี่ยสะสม:",
              style: TextStyle(
                fontSize: 15,
              )),
          SizedBox(width: 39),
          Text("4.00"),
        ]),
        SizedBox(height: 10),
      ],
    );
  }
}
