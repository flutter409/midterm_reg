import 'package:flutter/material.dart';
import '../main.dart';

void main() => runApp(MyNotification());

class MyNotification extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          ListTile(
            leading: Icon(
              Icons.arrow_back,
            ),
            title: const Text('การแจ้งเตือน',
                style: TextStyle(
                  fontSize: 20,
                )),
            // subtitle: Text("ย้อนกลับไปยังหน้าแรก"),
            onTap: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => HomePage2()),
              );
            },
          ),
          ListTile(
            leading: Icon(
              Icons.newspaper,
              color: Colors.amber,
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("มหาลัยบูรพา ขอเชิญชวนคณาจารย์/ผู้รับผิดชอบหลักสูตร",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text(
                    "และผู้สนใจทั่วไปเข้าร่วมโครงการอบรมเชิงปฎิบัติ เรื่อง.....",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text("31 ม.ค. 2566 15:02",
                    style: TextStyle(
                      fontSize: 10,
                    )),
              ],
            ),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            leading: Icon(
              Icons.book,
              color: Colors.amber,
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("31 ม.ค. 2566 เวลา 13:00-14:50 น. วิชา",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text("การวิคราะห์และออกแบบเชิงวัตถุ ณ ห้อง: IF-3CO1",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text("31 ม.ค. 2566 13:11",
                    style: TextStyle(
                      fontSize: 10,
                    )),
              ],
            ),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            leading: Icon(
              Icons.newspaper,
              color: Colors.amber,
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("ขอเชิญชวนคณาจารย์และผู้สนใจรับฟังการบรรยายพิเศษ",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text(
                    "เรื่องพลิกแนวคิดการเขียนหนังสือและตำราเพื่อเข้าสู่ตำ.....",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text("30 ม.ค. 2566 11:36",
                    style: TextStyle(
                      fontSize: 10,
                    )),
              ],
            ),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            leading: Icon(
              Icons.newspaper,
              color: Colors.amber,
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("มหาลัยบูรพา ขอเชิญชวนคณาจารย์/ผู้รับผิดชอบหลักสูตร",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text(
                    "และผู้สนใจทั่วไปเข้าร่วมโครงการอบรมเชิงปฎิบัติ เรื่อง.....",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text("30 ม.ค. 2566 08:15",
                    style: TextStyle(
                      fontSize: 10,
                    )),
              ],
            ),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            leading: Icon(
              Icons.newspaper,
              color: Colors.amber,
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("สรุปเหรียญรางวัลของ มหาลัยบรูพา การแข่งขันกีฬา",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text("มหาลัยแห่งประเทศไทย ครั้งที่ 48 (ดอกจานบ้าน.....",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text("29 ม.ค. 2566 20:01",
                    style: TextStyle(
                      fontSize: 10,
                    )),
              ],
            ),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            leading: Icon(
              Icons.newspaper,
              color: Colors.amber,
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("มหาลัยบูรพา ขอเชิญชวนคณาจารย์/ผู้รับผิดชอบหลักสูตร",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text(
                    "และผู้สนใจทั่วไปเข้าร่วมโครงการอบรมเชิงปฎิบัติ เรื่อง.....",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text("29 ม.ค. 2566 10:50",
                    style: TextStyle(
                      fontSize: 10,
                    )),
              ],
            ),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            leading: Icon(
              Icons.book,
              color: Colors.amber,
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("29 ม.ค. 2566 เวลา 15:00-16:50 น. วิชา",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text("การทดสอบซอฟต์แวร์ ณ ห้อง: IF-3C03",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text("29 ม.ค. 2566 12:00",
                    style: TextStyle(
                      fontSize: 10,
                    )),
              ],
            ),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            leading: Icon(
              Icons.newspaper,
              color: Colors.amber,
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("คณะบริหารธุกิจ มหาวิทยาลัยบูรพา ขอเชิญผู้สนใจเข้าร่วม",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text("กิจกรรม Lunch SET MENU ในเดือนแห่งความรัก.....",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text("28 ม.ค. 2566 17:30",
                    style: TextStyle(
                      fontSize: 10,
                    )),
              ],
            ),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            leading: Icon(
              Icons.newspaper,
              color: Colors.amber,
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("มหาลัยบูรพา ขอเชิญชวนคณาจารย์/ผู้รับผิดชอบหลักสูตร",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text(
                    "และผู้สนใจทั่วไปเข้าร่วมโครงการอบรมเชิงปฎิบัติ เรื่อง.....",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text("28 ม.ค. 2566 09:37",
                    style: TextStyle(
                      fontSize: 10,
                    )),
              ],
            ),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            leading: Icon(
              Icons.newspaper,
              color: Colors.amber,
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("สถาบันขงจื่อและศูนย์จีนศึกษา มหาลัยบรูพา ขอเชิญร่วม",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text("งานเฉลิมฉลองเทศกาล ตรุษจีนปีกระต่าย 2023 ร่วม....",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text("27 ม.ค. 2566 19:58",
                    style: TextStyle(
                      fontSize: 10,
                    )),
              ],
            ),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            leading: Icon(
              Icons.newspaper,
              color: Colors.amber,
            ),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("องค์ความรู้สู่การพัฒนาอย่างยั่งยืน ขอบคุณ สำนักงาน",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text("ประชาสัมพันธ์จังหวัดชลบุรีครับ #BUUtheonefamily...",
                    style: TextStyle(
                      fontSize: 13,
                    )),
                Text("26 ม.ค. 2566 14:05",
                    style: TextStyle(
                      fontSize: 10,
                    )),
              ],
            ),
            onTap: () {},
          ),
        ],
      ),
    );
  }
}
