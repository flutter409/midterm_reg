import 'package:flutter/material.dart';
import '../main.dart';

class timetable extends StatefulWidget{
  @override
  _timetable createState() =>  _timetable();
}

class _timetable extends State<timetable>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
        leading:  IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        title: Text('ตารางเรียน',style: TextStyle(fontSize: 20 ,)),
      ),
      body: TableWidget(),

      backgroundColor: Colors.white,
    );
  }
}

Widget TableWidget() {
  return  ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
                height: 40,
                child : Row(
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          "ตารางเรียน",
                          style: TextStyle(fontSize: 20),
                        )
                    ),
                  ],
                )
            ),
            Divider(
              color: Colors.grey,
            ),
            dataTable1(),
          ],
        ) ,
        Container(
            height: 40,
            child : Row(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "ตารางสอบ",
                      style: TextStyle(fontSize: 20,),
                    )
                ),
              ],
            )
        ),
        Divider(
          color: Colors.grey,
        ),
        Container(
          height: 1,
        ),
        dataTable2(),
      ]
  );
}

Widget dataTable1() {
  return ListTile(
    subtitle: Container(
      child:  Container(
        width: double.infinity,
        //Height constraint at Container widget level
        height: 150,
        child: Image.asset("./images/t1.png"),
      ),
    ),
  );
}

Widget dataTable2() {
  return ListTile(
    subtitle: Container(
      child:  Container(
        width: double.infinity,
        //Height constraint at Container widget level
        height: 250,
        child: Image.asset("./images/t2.png"),
      ),
    ),
  );
}