import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'menu.dart';
import 'noti.dart';
import 'login.dart';

void main() {
  runApp(DevicePreview(
    enabled: true,
    builder: (BuildContext context) => HomePage(),
  ));
}

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      debugShowCheckedModeBanner: false,
      home: Login(),
    );
  }
}

class HomePage2 extends StatelessWidget {
  const HomePage2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBarWidget(context),
      body: buildBodyWidget(),
      drawer: MyMenu(),
    );
  }
}

AppBar buildAppBarWidget(BuildContext context) {
  return AppBar(
      centerTitle: true,
      backgroundColor: Colors.white38,
      leading: Builder(
        builder: (BuildContext context) {
          return IconButton(
            icon: const Icon(
              Icons.menu,
              color: Color.fromARGB(255, 0, 0, 0),
              // size: 40, // Changing Drawer Icon Size
            ),
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
            tooltip: 'เมนู',
          );
        },
      ),
      title: Container(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "มหาวิทยาลัยบูรพา",
                style: TextStyle(fontSize: 20, color: Colors.black),
              ),
            ],
          ),
        ],
      )),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.notification_add),
          color: Colors.black,
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => MyNotification()),
            );
          },
          tooltip: 'แจ้งเตือน',
        ),
      ]);
}

//ทิ้งงงงงงงงง
var buildAppBarWidget1 = AppBar(
  leading: Builder(
    builder: (BuildContext context) {
      return IconButton(
        icon: const Icon(Icons.notification_add, color: Colors.black
            // size: 40, // Changing Drawer Icon Size
            ),
        onPressed: () {
          Scaffold.of(context).openDrawer();
        },
        tooltip: 'เมนู',
      );
    },
  ),
);

Widget buildBodyWidget() {
  return ListView(children: <Widget>[
    Column(
      children: <Widget>[
        Container(
            height: 40,
            child: Row(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "ข่าวสาร",
                      style: TextStyle(fontSize: 20),
                    )),
              ],
            )),
        Divider(
          color: Colors.grey,
        ),
        Container(
          // color: Colors.indigo.withOpacity(0.2),
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                news1(),
                news2(),
                news3(),
                news4(),
                news5(),
              ],
            ),
          ),
        ),
      ],
    ),
    Container(
        height: 40,
        child: Row(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "ประกาศ",
                  style: TextStyle(
                    fontSize: 20,
                  ),
                )),
          ],
        )),
    Divider(
      color: Colors.grey,
    ),
    noti1(),
    Divider(
      color: Colors.grey,
    ),
    noti2(),
    Divider(
      color: Colors.grey,
    ),
    noti3(),
    Divider(
      color: Colors.grey,
    ),
    noti4(),
    Divider(
      color: Colors.grey,
    ),
    noti5(),
    Divider(
      color: Colors.grey,
    ),
    noti6(),
  ]);
}

///xxxxx///////
Widget news1() {
  return Padding(
    padding: const EdgeInsets.all(1.0),
    child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(1.0),
          child: new Container(
            width: 200,
            height: 150,
            child: Image.asset("./images/f1.png"),
          ),
        ),
      ],
    ),
  );
}

Widget news2() {
  return Padding(
    padding: const EdgeInsets.all(1.0),
    child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(1.0),
          child: new Container(
            width: 200,
            height: 125,
            child: Image.asset("./images/f2.png"),
          ),
        ),
      ],
    ),
  );
}

Widget news3() {
  return Padding(
    padding: const EdgeInsets.all(1.0),
    child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(1.0),
          child: Container(
            width: 200,
            height: 125,
            child: Image.asset("./images/f3.png"),
          ),
        ),
      ],
    ),
  );
}

Widget news4() {
  return Padding(
    padding: const EdgeInsets.all(1.0),
    child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(1.0),
          child: new Container(
            width: 200,
            height: 125,
            child: Image.asset("./images/f4.png"),
          ),
        ),
      ],
    ),
  );
}

Widget news5() {
  return Padding(
    padding: const EdgeInsets.all(1.0),
    child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(1.0),
          child: new Container(
            width: 200,
            height: 125,
            child: Image.asset("./images/f5.png"),
          ),
        ),
      ],
    ),
  );
}

Widget noti1() {
  return ListTile(
    title: Text("การยื่นคำร้องขอสำเร็จการศึกษา ภาคปลาย ปีการศึกษา 2565",
        style: TextStyle(
          fontSize: 13,
        )),
    subtitle: Container(
      child: Container(
        width: double.infinity,
        //Height constraint at Container widget level
        height: 200,
        child: Image.asset("./images/n1.png"),
      ),
    ),
  );
}

Widget noti2() {
  return ListTile(
    title: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("กำหนดการชำระค่าธรรมเนียมการลงทะเบียนเรียนภาคปลาย",
            style: TextStyle(
              fontSize: 13,
            )),
        Text("ปีการศึกษา 2565",
            style: TextStyle(
              fontSize: 13,
            )),
      ],
    ),
    subtitle: Container(
      child: Container(
        width: double.infinity,
        //Height constraint at Container widget level
        height: 200,
        child: Image.asset("./images/n2.png"),
      ),
    ),
  );
}

Widget noti3() {
  return ListTile(
    title: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("กำหนดยื่นคำร้องเกี่ยวกับงานพิธีพระราชทานปริญญาบัตร",
            style: TextStyle(
              fontSize: 13,
            )),
        Text("ปีการศึกษา 2563 และปีการศึกษา 2564",
            style: TextStyle(
              fontSize: 13,
            )),
      ],
    ),
    subtitle: Container(
      child: Container(
        width: double.infinity,
        height: 250,
        child: Image.asset("./images/n3.png"),
      ),
    ),
  );
}

Widget noti4() {
  return ListTile(
    title: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("แบบประเมินความคิดเห็นต่อการให้บริการของสำนักงานอธิการบดี",
            style: TextStyle(
              fontSize: 13,
            )),
        Text(" - สำหรับนิสิต ที่ https://bit.ly/3cyvuuf",
            style: TextStyle(
              fontSize: 10,
            )),
        Text(" - สำหรับผู้ปกครอง ที่ https://bit.ly/39uIUWa",
            style: TextStyle(
              fontSize: 10,
            )),
      ],
    ),
    subtitle: Container(
      child: Container(
        width: double.infinity,
        height: 200,
        child: Image.asset("./images/n4.png"),
      ),
    ),
  );
}

Widget noti5() {
  return ListTile(
    title: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("LINE Official",
            style: TextStyle(
              fontSize: 13,
            )),
        Text(
            "   เพิ่มเพื่อน LINE Official ของกองทะเบียนฯ พิมพ์ @regbuu (ใส่ @ ด้วย) หรือ",
            style: TextStyle(
              fontSize: 10,
            )),
        Text("   https://lin.ee/uNohJQP หรือสแกน Qrcode ด้านล่าง",
            style: TextStyle(
              fontSize: 10,
            )),
      ],
    ),
    subtitle: Container(
      child: Container(
        width: double.infinity,
        height: 200,
        child: Image.asset("./images/n5.png"),
      ),
    ),
  );
}

Widget noti6() {
  return ListTile(
    title: Text("Download ระเบียบสำหรับแนบเบิกค่าเล่าเรียนบุตร",
        style: TextStyle(
          fontSize: 13,
        )),
    subtitle: Container(
      child: Container(
        width: double.infinity,
        height: 100,
        child: Image.asset("./images/n6.png"),
      ),
    ),
  );
}
